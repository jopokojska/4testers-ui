import pytest
from selenium.webdriver.common.by import By
from faker import Faker
from fixtures.chrome import chrome_browser
from pages.admin_page import AdminPage
from pages.create_project_page import CreateProjectPage
from pages.login_page import LoginPage
from pages.home_page import HomePage

administrator_email = 'administrator@testarena.pl'


@pytest.fixture
def browser(chrome_browser):
    chrome_browser.set_window_size(1920, 1080)
    chrome_browser.get('http://demo.testarena.pl/zaloguj')
    login_page = LoginPage(chrome_browser)
    login_page.attempt_login('administrator@testarena.pl', 'sumXQQ72$L')
    yield chrome_browser


@pytest.fixture(scope='module')
def generate_project_data():
    fake = Faker()
    return {
        'name': fake.name(),
        'prefix': fake.city_suffix(),
        'description': fake.catch_phrase()
    }


def test_successful_login(browser):
    home_page = HomePage(browser)
    user_email = home_page.get_current_user_email()
    assert administrator_email == user_email


def test_redirecting_to_admin_panel(browser):
    admin_panel_page = AdminPage(browser)
    admin_panel_page.navigate_to_projects()
    assert browser.title == 'Projekty - TestArena Demo'


def test_create_new_project(browser, generate_project_data):
    project_data = generate_project_data
    admin_panel_page = AdminPage(browser)
    admin_panel_page.navigate_to_projects()
    admin_panel_page.open_create_project_page()
    create_new_project_page = CreateProjectPage(browser)
    create_new_project_page.create_new_project(project_data['name'], project_data['prefix'], project_data['description'])
    assert browser.title == 'Właściwości projektu - TestArena Demo'


def test_search_for_the_created_project(browser, generate_project_data):
    project_name = generate_project_data['name']
    admin_panel_page = AdminPage(browser)
    admin_panel_page.navigate_to_projects()
    admin_panel_page.search_project(project_name)
    table_elements = browser.find_elements(By.CSS_SELECTOR, "td")
    assert check_text_presence(table_elements, project_name)


def check_text_presence(elements, text):
    for element in elements:
        if text in element.text:
            return True
    return False
