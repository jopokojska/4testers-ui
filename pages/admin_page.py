from selenium.webdriver.common.by import By


class AdminPage:
    admin_button = (By.CSS_SELECTOR, "[title='Administracja']")
    create_project_button = (By.CSS_SELECTOR, "[href='http://demo.testarena.pl/administration/add_project']")
    searchbar = (By.CSS_SELECTOR, "input#search")
    search_button = (By.CSS_SELECTOR, "#j_searchButton")

    def __init__(self, browser):
        self.browser = browser

    def navigate_to_projects(self):
        self.browser.find_element(*self.admin_button).click()

    def open_create_project_page(self):
        self.browser.find_element(*self.create_project_button).click()

    def search_project(self, project_name):
        self.browser.find_element(*self.searchbar).send_keys(project_name)
        self.browser.find_element(*self.search_button).click()



