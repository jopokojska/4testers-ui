from selenium.webdriver.common.by import By


class CreateProjectPage:
    project_name = (By.CSS_SELECTOR, "input#name")
    project_prefix = (By.CSS_SELECTOR, "#prefix")
    project_desc = (By.CSS_SELECTOR, "#description")
    save_button = (By.CSS_SELECTOR, "#save")

    def __init__(self, browser):
        self.browser = browser

    def create_new_project(self, name, prefix, description):
        self.browser.find_element(*self.project_name).send_keys(name)
        self.browser.find_element(*self.project_prefix).send_keys(prefix)
        self.browser.find_element(*self.project_desc). send_keys(description)
        self.browser.find_element(*self.save_button).click()
